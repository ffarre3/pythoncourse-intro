"""
Exercise warmup2
-----------------

Simple exercises to get used to python basics (continued).

- From http://introtopython.org/lists_tuples.html , do the following:
  - first list
  - working list
  - starting from empty
  - ordered numbers (ignore the requirement of using for loops)
  - alphabet slices
  - first twenty

- And *also* the following exercise on dictionaries:
  Create the following dictionary to be used as a phonebook:

        phonebook = { "John" : 7566,  "Jack" : 7264,  "Jill" : 2781}

  Add "Jake" to the phonebook with the phone number 938273443,  and remove Jill
  from the phonebook. Then print the phonebook in alphabetical order
"""

# Write your solution here
a=['stone','brick','tree','plant','1','55']
print (a[1])
b=a
print(b)
a[0]='marble'
print(a)                    #retrun 'marble','brick','tree','plant','1','55'
a.extend([6,8,99])          #return 'stone','brick','tree','plant','1','55','6','8','99'
print(a)
a.reverse()
print(a)
b.remove('brick')
print(b)                    #return [800, 6, 6, '1', 'plant', 'tree', 'marble']
b.insert(5,"ones")
print(b)                    #return [800, 6, 6, '1', 'plant', 'ones', 'tree', 'marble']
CONSTANT_1=45
print(CONSTANT_1+50)        #return 95
print(b[2:-2])              #Return [6, '55', '1', 'plant', 'tree']
print (a[1:-2:2])           #return [8, '55', 'plant']

list(a)                     #return [99, 8, 6, '55', '1', 'plant', 'tree', 'brick', 'marble']
print(a[1:-2:2])            #return [8, '55', 'plant']
#print (a.index(1))          #return not is in the list, because integer 1 do not exist
print (a.index('1'))        #return 5 (position 5)
a[0]=800
print(a)                    #replace a[0] for new number

import copy
c=copy.copy(a)
print(c)                    #now c is the same than a
a[1]=6
a[3]=6
print(a.count(6))           #return 3

ord('A')                    #return 65

d=["The","red","elephant"]
z,x,y=d
print(z)                    #return 'The'
print(y)                    #return 'elephant'
print(x)                    #return 'red'


a=1,2,'3','4','cat','dog'
print (a[2])                #return '3'
#a[2]="tiger"                #return error, because the tuple not is possible to modify.
print(a.count('cat'))       #return 1


phonebook = { "John" : 7566,  "Jack" : 7264,  "Jill" : 2781}
print(phonebook['John'])      #return 7566
phonebook['John']=7599
print(phonebook['John'])      #return 7599
del phonebook['Jill']
print(phonebook)              #return {'John': 7599, 'Jack': 7264}
phonebook['Jake']='938585566'
print(phonebook)              #return {'John': 7599, 'Jack': 7264, 'Jake': '938585566'}
