"""
Exercise warmup1
-----------------

Simple exercises to get used to python basics.

- From http://introtopython.org/var_string_num.html , do the following:
  - hello world
  - one variable, two messages
  - first name cases
  - full name
  - arithmetic

"""

# -->Write your solution here
# hello world
a='hello'
b='world'
print(a+' '+b)          #returns hello world

#one variable, two messages
c='Hello'
print(c)                #returns Hello
c='World'
print(c)                #returns World

#first name cases
a='hello'
print(a)                #returns hello
print(a.title)          #returns Hello
print(a.capitalize)     #returns Hello
print(a.upper)          #returns HELLO
print(a.lower)          #returns hello

# - full name
a='hello'
b='world'
c=a+' '+b
print(c)
len(c)                  #returns 11
'hello'=='Hello'        #return False
'hello'>'Hello'         #return True
c[0]                    #return h
c[-1]                   #return d
len(c)/2                #return 5.5
c[2:4]                  #return ll
c[1:-1]                 #return ello worl"

c[1:6:2]                #Why return 'el'
c[1:6:3]                #Why return 'eo'
c[1:6:1]                #Why return 'ello'

#arithmetic
a=1
b=2
print(a+b)              #return 3
print(a/b)              #reteun 0.5
print(int(a/b))         #reteun 0
print(a*b)              #reteun 2
print(a**b)             #reteun 1
print(float(a/b))       #return 0.5
print(abs(b-a))         #return 1
print(float(abs(b-a)))  #return 1.0
print(0.1+0.2)          #why return 0.30000000000000004
